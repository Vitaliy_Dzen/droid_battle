package com.epam.droidBattle.weapon.archer;

import com.epam.droidBattle.weapon.Weapon;

/**
 * Лучник
 */
public abstract class ArcherWeapon extends Weapon {

  public ArcherWeapon(int stamina, int damage) {
    super(stamina, damage);
  }
}
