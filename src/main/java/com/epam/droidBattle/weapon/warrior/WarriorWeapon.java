package com.epam.droidBattle.weapon.warrior;

import com.epam.droidBattle.weapon.Weapon;

/**
 * Воїн
 */
public abstract class WarriorWeapon extends Weapon {

  public WarriorWeapon(int stamina, int damage) {
    super(stamina, damage);
  }
}
