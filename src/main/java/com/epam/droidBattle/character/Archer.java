package com.epam.droidBattle.character;

import com.epam.droidBattle.enums.TypeOfCharacter;

public class Archer extends Character {

  public Archer(String name) {
    super(name, TypeOfCharacter.ARCHER);
  }

  public Enum getType() {
    return TypeOfCharacter.ARCHER;
  }
}
