package com.epam.droidBattle.game;

import com.epam.droidBattle.character.Archer;
import com.epam.droidBattle.character.Character;
import com.epam.droidBattle.character.Mage;
import com.epam.droidBattle.character.Warrior;
import com.epam.droidBattle.utilities.Draw;
import com.epam.droidBattle.utilities.DrawImage;
import com.epam.droidBattle.weapon.Weapon;
import com.epam.droidBattle.weapon.archer.Crossbow;
import com.epam.droidBattle.weapon.archer.HunterBow;
import com.epam.droidBattle.weapon.archer.Longbow;
import com.epam.droidBattle.weapon.mage.Electricity;
import com.epam.droidBattle.weapon.mage.Fire;
import com.epam.droidBattle.weapon.mage.Ice;
import com.epam.droidBattle.weapon.warrior.Axe;
import com.epam.droidBattle.weapon.warrior.Mace;
import com.epam.droidBattle.weapon.warrior.Sword;

import static com.epam.droidBattle.utilities.Utillites.scanInt;
import static com.epam.droidBattle.utilities.Utillites.scanString;

/** Chose a com.epam.droidBattle.character, choose com.epam.droidBattle.weapon... */
public class ChooseCharacterMenu {

  Character character;

  public ChooseCharacterMenu() {
    startMenu();
  }

  public void startMenu() {
    System.out.println("Fill in name of your character");
    String nameOfCharacter = scanString();

    printChoseCharacter();
    switch (scanInt()) {
      case 1:
        character = new Archer(nameOfCharacter);
        weaponForArcher();
        break;
      case 2:
        character = new Mage(nameOfCharacter);
        weaponForMage();
        break;
      case 3:
        character = new Warrior(nameOfCharacter);
        weaponForWarrior();
        break;
    }
  }

  private void weaponForArcher() {
    Weapon weapon = null;
    System.out.println(
        "Please chose yours weapon\n" + "1 - Crossbow\n" + "2 - HunterBow\n" + "3 - Longbow");

    switch (scanInt()) {
      case 1:
        weapon = new Crossbow();
        break;
      case 2:
        weapon = new HunterBow();
        break;
      case 3:
        weapon = new Longbow();
        break;
    }
    character.addWeaponInventory(weapon);
  }

  private void weaponForMage() {
    Weapon weapon = null;
    System.out.println(
        "Please chose yours weapon\n" + "1 - Electricity\n" + "2 - Fire\n" + "3 - Ice");

    switch (scanInt()) {
      case 1:
        weapon = new Electricity();
        break;
      case 2:
        weapon = new Fire();
        break;
      case 3:
        weapon = new Ice();
        break;
    }
    character.addWeaponInventory(weapon);
  }

  private void weaponForWarrior() {
    Weapon weapon = null;
    System.out.println("Please chose yours weapon\n" + "1 - Axe\n" + "2 - Mace\n" + "3 - Sword");
    switch (scanInt()) {
      case 1:
        weapon = new Axe();
        break;
      case 2:
        weapon = new Mace();
        break;
      case 3:
        weapon = new Sword();
        break;
    }
    character.addWeaponInventory(weapon);
  }

  private void printChoseCharacter() {
    System.out.println("Please chose yours character");
    Draw.printTextArcher();
    DrawImage.printArcher2_1();
    DrawImage.printArcher1_0();

    Draw.printTextMage();
    DrawImage.printMage1_0();
    DrawImage.printMage2_1();

    Draw.printTextWarrior();
    DrawImage.printWarrior1_0();
    DrawImage.printWarrior2_1();

    System.out.println(
        "Please chose yours character\n" + "1 - Archer\n" + "2 - Mage\n" + "3 - Warrior\n");
  }

  /** print псевдографічно героя */
  private void printCharacter() {}

  /** print intro about game */
  private void printIntro() {}

  public void setCharacter(Character character) {
    this.character = character;
  }

  public Character getCharacter() {
    return character;
  }
}
