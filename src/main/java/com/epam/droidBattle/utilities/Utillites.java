package com.epam.droidBattle.utilities;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Utillites {

  public static int scanInt() {
    try {
      return new Scanner(System.in).nextInt();
    } catch (InputMismatchException e) {
      System.out.println("Only numbers are acceptable, please try again.");
      return scanInt();
    }
  }

  public static String scanString() {
    return new Scanner(System.in).next();
  }
}
