package com.epam.droidBattle.character;

import com.epam.droidBattle.enums.TypeOfCharacter;

public class Mage extends Character {

  public Mage(String name) {
    super(name, TypeOfCharacter.MAGE);
  }

  public Enum getType() {
    return TypeOfCharacter.MAGE;
  }
}
