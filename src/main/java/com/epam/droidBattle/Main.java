package com.epam.droidBattle;

import com.epam.droidBattle.game.Battle;
import com.epam.droidBattle.game.ChooseCharacterMenu;
import com.epam.droidBattle.utilities.Draw;

import static com.epam.droidBattle.utilities.Color.GREEN;
import static com.epam.droidBattle.utilities.Color.RED;
import static com.epam.droidBattle.utilities.Color.toColorLn;

public class Main {
  public static void main(String[] args) {
    Draw.example1();

    ChooseCharacterMenu player1 = new ChooseCharacterMenu();
    toColorLn(RED, "**************************************************************");
    toColorLn(GREEN, "2 Player");
    toColorLn(RED, "**************************************************************");
    ChooseCharacterMenu player2 = new ChooseCharacterMenu();

    Battle battle = new Battle(player1.getCharacter(), player2.getCharacter());
    battle.startBattle();


  }
}
