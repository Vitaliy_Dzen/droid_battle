package com.epam.droidBattle.game;

import com.epam.droidBattle.character.Character;
import com.epam.droidBattle.utilities.Draw;

import static com.epam.droidBattle.utilities.Color.*;

public class Battle {

  private Character userFirst;
  private Character userSecond;

  public Battle(Character userFirst, Character userSecond) {
    this.userFirst = userFirst;
    this.userSecond = userSecond;
  }

  public void startBattle() {
    while (userFirst.getHealthpoints() > 0 && userSecond.getHealthpoints() > 0) {
      toColorLn(CYAN, "1 player - " + userFirst.getName());
      userFirst.takeStep(userSecond);
      showStatus();

      if (userFirst.getHealthpoints() <= 0 || userSecond.getHealthpoints() <= 0) {
        break;
      }
      toColorLn(BLUE, "2 player - " + userSecond.getName());
      userSecond.takeStep(userFirst);
      showStatus();
    }
    printWinner();
  }

  private void showStatus() {
    toColorLn(GREEN, "*******************************************************************************************\n"+
  "*First player "+userFirst.getName()+":\n"+
   "*" + userFirst.toString());
            toColorLn(RED, "*******************************************************************************************");
    toColorLn(YELLOW, "*Second player "+userSecond.getName()+":"+
  "*" + userSecond.toString()+
    "\n*******************************************************************************************");
  }

  private void printWinner() {
    if (userFirst.getHealthpoints() <= 0) {
      toColorLn(RED, "Second player " + userSecond.getName() + " is Win");
      Draw.printTextGameOver();
    } else if (userSecond.getHealthpoints() <= 0) {
      toColorLn(RED, "First player " + userFirst.getName() + " is Win");
      userFirst.setCountOfFight(userFirst.getCountOfFight() + 1);
      Draw.printTextGameOver();
      /*
       *Shop shop...
       */
    }
  }

  //    private void step

}
