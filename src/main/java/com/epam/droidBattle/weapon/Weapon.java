package com.epam.droidBattle.weapon;

public abstract class Weapon {

  private final int stamina;
  private final int damage;

  public Weapon(int stamina, int damage) {
    this.stamina = stamina;
    this.damage = damage;
  }

  public int getStamina() {
    return stamina;
  }

  public int getDamage() {
    return damage;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Weapon ");
    sb.append("stamina - ").append(stamina);
    sb.append(", damage - ").append(damage);
    return sb.toString();
  }

//  @Override
//  public String toString() {
//    final StringBuilder sb = new StringBuilder("Weapon{");
//    sb.append("stamina=").append(stamina);
//    sb.append(", damage=").append(damage);
//    sb.append('}');
//    return sb.toString();
//  }
}
