package com.epam.droidBattle.weapon.warrior;

/**
 * Меч
 */
public class Sword extends WarriorWeapon{

  public Sword() {
    super(10, 15);
  }
}
