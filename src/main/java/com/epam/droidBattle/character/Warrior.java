package com.epam.droidBattle.character;

import com.epam.droidBattle.enums.TypeOfCharacter;

public class Warrior extends Character {

  public Warrior(String name) {
    super(name, TypeOfCharacter.WARRIOR);
  }

  public Enum getType() {
    return TypeOfCharacter.WARRIOR;
  }
}
