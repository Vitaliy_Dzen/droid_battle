package com.epam.droidBattle.weapon.archer;

/**
 * ]Довгий 'англійськи' лук
 */
public class Longbow extends ArcherWeapon {

  public Longbow() {
    super(20, 25);
  }
}
