package com.epam.droidBattle.weapon.archer;

/**
 * Арбалет
 */
public class Crossbow extends ArcherWeapon {

  public Crossbow() {
    super(10, 15);
  }
}
