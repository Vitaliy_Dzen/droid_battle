package com.epam.droidBattle.character;

import com.epam.droidBattle.game.Shop;
import com.epam.droidBattle.weapon.Weapon;

import java.util.ArrayList;
import java.util.List;

import static com.epam.droidBattle.utilities.Color.RED;
import static com.epam.droidBattle.utilities.Color.toColorLn;
import static com.epam.droidBattle.utilities.Utillites.scanInt;

public abstract class Character {

  private String name;
  private int healthpoints = 150;
  private int stamina = 100;
  private Enum type;
  private int cash = 0;
  private int countOfFight = 0; // кількість боїв

  private List<Weapon> weaponInventory;
  private int medkitInventory;
  private int potionInventory;

  public Character(String name, Enum type) {
    this.name = name;
    this.healthpoints = 150;
    this.stamina = 100;
    this.weaponInventory = new ArrayList<>();
    this.medkitInventory = 3;
    this.potionInventory = 2;
    this.type = type;
  }

  public void takeStep(Character enemy) {
    System.out.println("Choose your action!");
    System.out.println(
        "1 - attack \n 2 - use medkit \n 3 - use potion \n 4 - wait  \n 5 - retreat");
    switch (scanInt()) {
      case 1:
        attack(chooseYourWeapon(), enemy);
        break;
      case 2:
        useMedkit();
        break;
      case 3:
        usePotion();
        break;
      case 4:
        break;
      case 5:
        toColorLn(RED, enemy.getName() + " won!");
        System.exit(1);
    }
  }

  private void attack(Weapon weapon, Character enemy) {
    if (this.stamina >= weapon.getStamina()) {
      this.stamina -= weapon.getStamina();
      enemy.takeHit(weapon.getDamage());
    } else {
      System.out.println("You cannot use this action\nSo you drank a potion");
      usePotion();
    }
  }

  private void useMedkit() {
    // this is out genereal health after we use a medkit
    int healthPlusMedkit = this.healthpoints + Shop.HELATH_RESTORATION;
    this.healthpoints = healthPlusMedkit < 150 ? healthPlusMedkit : 100;

    this.medkitInventory -= 1;
  }

  private void usePotion() {
    // this is out genereal stamina after we use a potion
    int staminaPlusPotion = this.stamina + Shop.STAMINA_RESTORATION;
    this.stamina = staminaPlusPotion < 100 ? staminaPlusPotion : 100;

    this.potionInventory -= 1;
  }

  private Weapon chooseYourWeapon() {
    if (weaponInventory.size() == 1) {
      return weaponInventory.get(0);

    } else {
      System.out.println("Choose you weapon");
      for (int i = 0; i < weaponInventory.size(); i++) {
        System.out.println("[ " + i + "] - " + weaponInventory.get(i));
      }
      // TODO tryCatch correct input data
      return weaponInventory.get(scanInt());
    }
  }

  public void takeHit(int damage) {
    this.healthpoints -= damage;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getHealthpoints() {
    return healthpoints;
  }

  public void setHealthpoints(int healthpoints) {
    this.healthpoints = healthpoints;
  }

  public int getStamina() {
    return stamina;
  }

  public void setStamina(int stamina) {
    this.stamina = stamina;
  }

  public Enum getType() {
    return type;
  }

  public void setType(Enum type) {
    this.type = type;
  }

  public int getMedkitInventory() {
    return medkitInventory;
  }

  public void setMedkitInventory(int medkitInventory) {
    this.medkitInventory = medkitInventory;
  }

  public int getPotionInventory() {

    return potionInventory;
  }

  public void setPotionInventory(int potionInventory) {
    this.potionInventory = potionInventory;
  }

  public void addWeaponInventory(Weapon weaponInventory) {
    this.weaponInventory.add(weaponInventory);
  }

  public List<Weapon> getWeaponInventory() {
    return weaponInventory;
  }

  public int getCash() {
    return cash;
  }

  public int getCountOfFight() {
    return countOfFight;
  }

  public void setCash(int cash) {
    this.cash = cash;
  }

  public void setCountOfFight(int countOfFight) {
    this.countOfFight = countOfFight;
  }


//  @Override
//  public String toString() {
//    return "Character{"
//        + "name='"
//        + name
//        + '\''
//        + ", healthpoints="
//        + healthpoints
//        + ", stamina="
//        + stamina
//        + ", type="
//        + type
//        + ", weaponInventory="
//        + weaponInventory
//        + ", medkitInventory="
//        + medkitInventory
//        + ", potionInventory="
//        + potionInventory
//        + '}';
//  }

  @Override
  public String toString() {
    return  "Name - '" + name + '\''+ ", type - "+ type+"\n"
            + "* HP - " + healthpoints
            + ", Stamina - " + stamina +
            ", Weapon - " + weaponInventory+"\n"
            + "* Medkit(s) - " + medkitInventory
            + ", Potion - " + potionInventory;


  }

}
