package com.epam.droidBattle.weapon.archer;

/**
 * Лук
 */
public class HunterBow extends ArcherWeapon {

  public HunterBow() {
    super(30, 35);
  }
}
