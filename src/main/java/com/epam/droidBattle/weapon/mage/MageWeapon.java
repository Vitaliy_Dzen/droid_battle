package com.epam.droidBattle.weapon.mage;

import com.epam.droidBattle.weapon.Weapon;

public abstract class MageWeapon extends Weapon {

  public MageWeapon(int stamina, int damage) {
    super(stamina, damage);
  }
}
